"use strict"

class Mat4{

    constructor(){
        this.m = new Float32Array(16);
        this.m[0] = 1;
        this.m[1] = 0;
        this.m[2] = 0;
        this.m[3] = 0;

        this.m[4] = 0;
        this.m[5] = 1;
        this.m[6] = 0;
        this.m[7] = 0; 

        this.m[8] = 0; 
        this.m[9] = 0; 
        this.m[10] = 1; 
        this.m[11] = 0; 

        this.m[12] = 0;
        this.m[13] = 0;
        this.m[14] = 0;
        this.m[15] = 1;
    }

    static fromArray(a){
        var mat = new Mat4();
        for(var i=0; i<16; i++){
            mat.m[i] = a[i];
        }
        return mat;
    }

    static lookAt(eye, target, up){
        var n = Vector.normalize3(Vector.sub(eye, target));
        var u = Vector.normalize3(Vector.cross3(up, n));
        var v = Vector.normalize3(Vector.cross3(n, u));

        var rt = new Mat4();
        rt.m[0] = u[0];
        rt.m[1] = v[0];
        rt.m[2] = n[0];

        rt.m[4] = u[1];
        rt.m[5] = v[1];
        rt.m[6] = n[1];

        rt.m[8] = u[2];
        rt.m[9] = v[2];
        rt.m[10] = n[2];

        rt.m[12] = - Vector.dot(u, eye);
        rt.m[13] = - Vector.dot(v, eye);
        rt.m[14] = - Vector.dot(n, eye);

        return rt;
    }

    static lookAt2(position, direction, up){
        var target = Vector.add(position, direction);
        return this.lookAt(position, target, up);
    }

    /**
    * Generates a perspective projection matrix with the given bounds.
    * Passing null/undefined/no value for far will generate infinite projection matrix.
    * 
    * Von gl-matrix übernommen und nicht verstanden
    *
    * @param {mat4} out mat4 frustum matrix will be written into
    * @param {number} fovy Vertical field of view in radians
    * @param {number} aspect Aspect ratio. typically viewport width/height
    * @param {number} near Near bound of the frustum
    * @param {number} far Far bound of the frustum, can be null or Infinity
    * @returns {mat4} out
    */
    static perspective(fovy, aspect, near, far) {
        var out = new Mat4();

        // berechnet höhe -> top = -bottom
        var f = 1.0 / Math.tan(fovy / 2),
            nf;
        out.m[0] = f / aspect;
        out.m[5] = f;
        out.m[11] = -1;
        out.m[15] = 0;

        // terme fallen weg, daher 12, 13 = 0

        if (far != null && far !== Infinity) {
        nf = 1 / (near - far);
        out.m[10] = (far + near) * nf;
        out.m[14] = 2 * far * near * nf;
        } else {
        out.m[10] = -1;
        out.m[14] = -2 * near;
        }

        return out;
    }


    toArray(){
        return this.m;
    }

    toString(){
        return (`[${this.m[0]}, ${this.m[4]}, ${this.m[8]}, ${this.m[12]},\n ${this.m[1]}, ${this.m[5]}, ${this.m[9]}, ${this.m[13]},\n ${this.m[2]}, ${this.m[6]}, ${this.m[10]}, ${this.m[14]},\n ${this.m[3]}, ${this.m[7]}, ${this.m[11]}, ${this.m[15]}]`);
    }

    clone(){
        var out = Mat4.fromArray(this.toArray());
        return out;
    }

    identity(){
        this.m[0] = 1;
        this.m[1] = 0;
        this.m[2] = 0;
        this.m[3] = 0;

        this.m[4] = 0;
        this.m[5] = 1;
        this.m[6] = 0;
        this.m[7] = 0; 

        this.m[8] = 0; 
        this.m[9] = 0; 
        this.m[10] = 1; 
        this.m[11] = 0; 

        this.m[12] = 0;
        this.m[13] = 0;
        this.m[14] = 0;
        this.m[15] = 1;

        return this;
    }

    add(b){
        this.m[0] += b.m[0];
        this.m[1] += b.m[1];
        this.m[2] += b.m[2];
        this.m[3] += b.m[3];

        this.m[4] += b.m[4];
        this.m[5] += b.m[5];
        this.m[6] += b.m[6];
        this.m[7] += b.m[7]; 

        this.m[8] += b.m[8]; 
        this.m[9] += b.m[9]; 
        this.m[10] += b.m[10]; 
        this.m[11] += b.m[11]; 

        this.m[12] += b.m[12];
        this.m[13] += b.m[13];
        this.m[14] += b.m[14];
        this.m[15] += b.m[15];

        return this;
    }

    sub(b){
        this.m[0] -= b.m[0];
        this.m[1] -= b.m[1];
        this.m[2] -= b.m[2];
        this.m[3] -= b.m[3];

        this.m[4] -= b.m[4];
        this.m[5] -= b.m[5];
        this.m[6] -= b.m[6];
        this.m[7] -= b.m[7]; 

        this.m[8] -= b.m[8]; 
        this.m[9] -= b.m[9]; 
        this.m[10] -= b.m[10]; 
        this.m[11] -= b.m[11]; 

        this.m[12] -= b.m[12];
        this.m[13] -= b.m[13];
        this.m[14] -= b.m[14];
        this.m[15] -= b.m[15];

        return this;
    }

    mul(b){
        var m = new Float32Array(this.m);
        this.m[0] = m[0] * b.m[0] + m[4] * b.m[1] + m[8] * b.m[2] + m[12] * b.m[3];
        this.m[1] = m[1] * b.m[0] + m[5] * b.m[1] + m[9] * b.m[2] + m[13] * b.m[3];
        this.m[2] = m[2] * b.m[0] + m[6] * b.m[1] + m[10] * b.m[2] + m[14] * b.m[3];
        this.m[3] = m[3] * b.m[0] + m[7] * b.m[1] + m[11] * b.m[2] + m[15] * b.m[3];

        this.m[4] = m[0] * b.m[4] + m[4] * b.m[5] + m[8] * b.m[6] + m[12] * b.m[7];
        this.m[5] = m[1] * b.m[4] + m[5] * b.m[5] + m[9] * b.m[6] + m[13] * b.m[7];
        this.m[6] = m[2] * b.m[4] + m[6] * b.m[5] + m[10] * b.m[6] + m[14] * b.m[7];
        this.m[7] = m[3] * b.m[4] + m[7] * b.m[5] + m[11] * b.m[6] + m[15] * b.m[7];

        this.m[8] = m[0] * b.m[8] + m[4] * b.m[9] + m[8] * b.m[10] + m[12] * b.m[11];
        this.m[9] = m[1] * b.m[8] + m[5] * b.m[9] + m[9] * b.m[10] + m[13] * b.m[11];
        this.m[10] = m[2] * b.m[8] + m[6] * b.m[9] + m[10] * b.m[10] + m[14] * b.m[11];
        this.m[11] = m[3] * b.m[8] + m[7] * b.m[9] + m[11] * b.m[10] + m[15] * b.m[11];

        this.m[12] = m[0] * b.m[12] + m[4] * b.m[13] + m[8] * b.m[14] + m[12] * b.m[15];
        this.m[13] = m[1] * b.m[12] + m[5] * b.m[13] + m[9] * b.m[14] + m[13] * b.m[15];
        this.m[14] = m[2] * b.m[12] + m[6] * b.m[13] + m[10] * b.m[14] + m[14] * b.m[15];
        this.m[15] = m[3] * b.m[12] + m[7] * b.m[13] + m[11] * b.m[14] + m[15] * b.m[15];

        return this;
    }

    scalar(s){
        for (var i=0; i<16;i++){
            this.m[i] *= s;
        }

        return this;
    }

    translate(v){
        var tv = new Mat4();
        tv.m[12] = v[0];
        tv.m[13] = v[1];
        tv.m[14] = v[2];
        this.mul(tv);

        return this;
    }

    rotate(angle, axis){
        var [rx, ry, rz] = axis;
        var rlen = 1 / Math.hypot(rx, ry, rz);
        rx *= rlen;
        ry *= rlen;
        rz *= rlen;

        var r = new Mat4();
        var cos = Math.cos(angle);
        var sin = Math.sin(angle);
        r.m[0] = cos + rx*rx * (1 - cos);
        r.m[1] = ry * rx * (1 - cos) + rz * sin;
        r.m[2] = rz * rx * (1 - cos) - ry * sin;

        r.m[4] = rx * ry * (1 - cos) - rz * sin;
        r.m[5] = cos + ry*ry * (1 - cos);
        r.m[6] = rz * ry * (1 - cos) + rx * sin;

        r.m[8] = rx * rz * (1 - cos) + ry * sin;
        r.m[9] = ry * rz * (1 - cos) - rx * sin;
        r.m[10] = cos + rz*rz * (1 - cos);
        
        this.mul(r);

        return this;
    }

    scale(vs){
        var [sx, sy, sz] = vs;
        this.m[0] *= sx;
        this.m[1] *= sx;
        this.m[2] *= sx;
        this.m[3] *= sx;

        this.m[4] *= sy;
        this.m[5] *= sy;
        this.m[6] *= sy;
        this.m[7] *= sy;

        this.m[8] *= sz;
        this.m[9] *= sz;
        this.m[10] *= sz;
        this.m[11] *= sz;

        return this;
        
    }
}

class Vector{

    static sub(a, b){
        var out = []
        for(var i=0; i<a.length; i++){
            out[i] = a[i] - b[i];
        }
        return out;
    }

    static add(a, b){
        var out = [];
        for (let i = 0; i < a.length; i++) {
            out[i] = a[i] + b[i];
        }
        return out;
    }

    static dot(a, b){
        var out = 0;
        for (var i=0; i<a.length; i++){
            out += a[i] * b[i];
        }
        return out;
    }

    static normalize3(a){
        var mag = Math.hypot(a[0], a[1], a[2]);
        for (let i = 0; i < a.length; i++) {
            a[i] /= mag;            
        }
        return a;
    }

    static cross3(a, b){
        var out = [];
        out[0] = a[1]*b[2] - a[2]*b[1];
        out[1] = a[2]*b[0] - a[0]*b[2];
        out[2] = a[0]*b[1] - a[1]*b[0];
        return out;
    }
}

function toRadian(angle){
    return angle * (Math.PI / 180);
}