"use strict"

var gl;
var canvas;

var BGColorCanvas = [0.07, 0.05, 0.09];

/**
 * Initialize WebGL2 (or WebGL.. or experimental-webgl) on the specified canvas
 * @param {HTMLCanvasElement} webgl_canvas 
 * @returns {WebGL2RenderingContext} 
 */
function init(webgl_canvas){
    canvas = webgl_canvas;
    gl = canvas.getContext("webgl2");
    var msg = "Using WebGL 2"

    if (!gl) {
        msg = "Using WebGL 1";
        gl = canvas.getContext('webgl');
    }

    if (!gl) {
        msg = "Using experimental-webgl"
        gl = canvas.getContext('experimental-webgl');
    }

    if (!gl){
        alert('Your browser does not support WebGL');
        return null;
    }

    console.log(msg);

    
    gl.enable(gl.DEPTH_TEST);

    gl.enable(gl.CULL_FACE);
    gl.cullFace(gl.BACK);
    gl.frontFace(gl.CCW);

    clear(BGColorCanvas);
    
    return gl;
}

/**
 * Clear the color and depth buffer
 * @param {number[]} color
 */
 function clear(color = null){
    if (color){
        gl.clearColor(color[0], color[1], color[2], color[3] != null ? color[3] : 1.0);
    }

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
}

/**
 * Create, link and validate a WebGLProgram using the supplied shader files
 * @param {string} vertexShaderPath 
 * @param {string} fragmentShaderPath 
 * @returns {Promise<WebGLProgram>}
 */
async function createProgram(vertexShaderPath, fragmentShaderPath){
    var vertexShader = await loadShader(gl.VERTEX_SHADER, vertexShaderPath);
    var fragmentShader = await loadShader(gl.FRAGMENT_SHADER, fragmentShaderPath);

    var program = gl.createProgram();
    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);
    gl.linkProgram(program);

    if (!gl.getProgramParameter(program, gl.LINK_STATUS)){
        console.error("Failed to link program: ", gl.getProgramInfoLog(program));
        return null;
    }

    gl.validateProgram(program);
    if (!gl.getProgramParameter(program, gl.VALIDATE_STATUS)) {
        console.error("Failed to validate program: ", gl.getProgramInfoLog(program));
        return null;
    }

    gl.useProgram(program);
    return program;
}

/**
 * Fetch a file and return it as a string
 * @param {string} url 
 * @returns {Promise<string>} File content as string
 */
async function loadFileAsString(url){
    var file = await fetch(url);

    if (!file.ok){
        console.error(`Failed to load ${url}: file not found`);
        return null;
    }
    var text = await file.text();
    return text;
}

/**
 * Fetch an external shader file and compile it.
 * @param {number} type 
 * @param {string} url 
 * @returns {Promise<WebGLShader>}
 */
async function loadShader(type, url){
    
    var shaderSource = await loadFileAsString(url);
    var shader = gl.createShader(type);
    gl.shaderSource(shader, shaderSource);
    gl.compileShader(shader);
    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)){
        console.error(`Failed to compile shader: ` + gl.getShaderInfoLog(shader));
        return null;
    }
    return shader;
}

/**
 * Parse a .obj file and produce a non-index vertex array:
 * @param {string} url path or url to .obj file 
 * @returns {Promise<Float32Array>} Vertex array in the following presentation: [x, y, z, u, v, nx, ny, nz]
 */

async function parseObj(url){

    var text = await loadFileAsString(url);
    var lines = text.split(/\r*\n/);

    var verts = [];
    var normals = [];
    var texCoords = [];
    var triangles = [];

    lines.forEach(line => {
        var data = line.trim().split(/\s+/);

        switch (data[0]) {
            case "v":
                verts.push(parseFloat(data[1]), parseFloat(data[2]), parseFloat(data[3]));
                break;
            case "vt":
                texCoords.push(parseFloat(data[1]), parseFloat(data[2]));
                break;

            case "vn":
                normals.push(parseFloat(data[1]), parseFloat(data[2]), parseFloat(data[3]))
                break;

            case "f":
                // split triangle indices into 3 sub arrays, each of the form [v, vt, vn]
                var ix = data.slice(1).map(item => item.split("/"))
                
                if (ix.length > 3){
                    console.error("Failed to parse .obj file: object is not triangulated");
                    return;
                }
                
                // reduce index value by one to make it start at 0
                ix = ix.map(item => item.map(item => parseInt(item) - 1));
                triangles.push(ix);
                break;
            default:
                break;
        }
    });

    console.log(`Read ${verts.length / 3} vertices and ${triangles.length} triangles`);


    // This is more flexible, but allocates an extra array
    var output = [];
    for (var i = 0; i < triangles.length; i++) {
        const face = triangles[i];
        for (var j = 0; j < 3; j++){
            output.push(
                verts[face[j][0] * 3],
                verts[face[j][0] * 3 + 1],
                verts[face[j][0] * 3 + 2],
                texCoords[face[j][1] * 2],
                texCoords[face[j][1] * 2 + 1],
                normals[face[j][2] * 3],
                normals[face[j][2] * 3 + 1],
                normals[face[j][2] * 3 + 2],
            )
        }
    }

    return output;
    
    /*
    // This does not allocate an extra array
    var f32out = new Float32Array(8 * 3 * triangles.length);
    for (var i = 0; i < triangles.length; i++) {
        
        // indices for a triangle, stored in 3 sub arrays [v, vt, vn]
        var triangle_indices = triangles[i];
        for (var j = 0; j < 3; j++){
            
            // offset at which to start inserting
            var fOffset = 8 * (i * 3 + j);
            
            f32out[fOffset] = verts[triangle_indices[j][0] * 3];
            f32out[fOffset + 1] = verts[triangle_indices[j][0] * 3 + 1];
            f32out[fOffset + 2] = verts[triangle_indices[j][0] * 3 + 2];
            f32out[fOffset + 3] = texCoords[triangle_indices[j][1] * 2];
            f32out[fOffset + 4] = texCoords[triangle_indices[j][1] * 2 + 1];
            f32out[fOffset + 5] = normals[triangle_indices[j][2] * 3];
            f32out[fOffset + 6] = normals[triangle_indices[j][2] * 3 + 1];
            f32out[fOffset + 7] = normals[triangle_indices[j][2] * 3 + 2];
            
        }
        
    }
    
    return f32out;
    */
    
}

/**
 * Load the specified OBJ file into the array buffer and link attributes
 * @param {string} url URL or path to .obj file
 * @param {number} posAttribLocation index of the vertex postion attribute in the program
 * @param {number} normalAttribLocation index of the normal attribute in the program
 * @param {number} texAttribLocation index of the texture coord attribute in the program
 * @returns {Promise<number>} The number of vertices in the model
 */
async function loadModel(url, posAttribLocation, normalAttribLocation = -1, texAttribLocation = -1){
    var obj = await parseObj(url);

    var buffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(obj), gl.STATIC_DRAW);

    gl.vertexAttribPointer(
        posAttribLocation,
        3,
        gl.FLOAT,
        gl.FALSE,
        8 * Float32Array.BYTES_PER_ELEMENT,
        0
    )
    gl.enableVertexAttribArray(posAttribLocation);
    
    if (normalAttribLocation != -1){
        gl.vertexAttribPointer(
            normalAttribLocation,
            3,
            gl.FLOAT,
            gl.FALSE,
            8 * Float32Array.BYTES_PER_ELEMENT,
            5 * Float32Array.BYTES_PER_ELEMENT
        )
        gl.enableVertexAttribArray(normalAttribLocation);
    }

    if (texAttribLocation != -1){
        gl.vertexAttribPointer(
            texAttribLocation,
            2,
            gl.FLOAT,
            gl.FALSE,
            8 * Float32Array.BYTES_PER_ELEMENT,
            3 * Float32Array.BYTES_PER_ELEMENT,     
        )
        gl.enableVertexAttribArray(texAttribLocation);
    }

    return obj.length / 8;

}

/**
 * Bind the supplied vertex array and optional index array
 * @param {number[]} vertexArray 
 * @param {number[]} indexArray 
 */
function bindVertices(vertexArray, indexArray = null){
    var vertexBuffer = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexArray), gl.STATIC_DRAW);

    if (indexArray){
        var indexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indexArray), gl.STATIC_DRAW);
    }
}

async function setup2DWorkspace(vertexShaderPath, fragmentShaderPath){
    var program = await createProgram(vertexShaderPath, fragmentShaderPath);
    var verts = [
        -1, -1, 0,
        1, -1, 0,
        -1, 1, 0,
        1, 1, 0
    ];
    bindVertices(verts);
    
    vpos = gl.getAttribLocation(program, "VERT_POSITION");
    gl.vertexAttribPointer(
        vpos,
        3,
        gl.FLOAT,
        gl.FALSE,
        3 * Float32Array.BYTES_PER_ELEMENT,
        0
    );
    gl.enableVertexAttribArray(vpos);

    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
    return program;

}

var utils = /*#__PURE__*/Object.freeze({
    __proto__: null,
    init: init,
    createProgram: createProgram,
    clear: clear,
    loadModel: loadModel,
    bindVertices: bindVertices,
    loadFileAsString: loadFileAsString,
    setup2DWorkspace: setup2DWorkspace
});